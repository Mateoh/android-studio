package com.huancho.app;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main5Activity extends AppCompatActivity{
    @BindView(R.id.btnregistrodeposito) Button buttonOne;
    @BindView(R.id.btnmostrarsaldo) Button buttontwo;

    @BindView(R.id.fecha_depos) EditText dato1;
    @BindView(R.id.tipo_depos) EditText dato2;
    @BindView(R.id.cant_deposit) EditText dato3;


    TextView datoww,datosaldo;
    String da1,da2,da3,dni_c,nomb,apell;
    int datt1=0,datt2=0;
    int saldo;
    Calendar calendarioone = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main5);
        ButterKnife.bind(this);
        datoww = (TextView)findViewById(R.id.deposito_clie);
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato22");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato33");
        datoww.setText(da2+" "+da3);

        datosaldo = (TextView) findViewById(R.id.deposito_clien_most);

        dato1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(Main5Activity.this, date, calendarioone
                        .get(Calendar.YEAR), calendarioone.get(Calendar.MONTH),
                        calendarioone.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }
    @OnClick(R.id.btnregistrodeposito)
    public void buttonOne(View view){
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this,"Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String fecha = dato1.getText().toString();
        String tipo = dato2.getText().toString();
        String cantidad = dato3.getText().toString();
        String dni = da1;
        ContentValues registro = new ContentValues();
        registro.put("fecha_d", fecha);
        registro.put("tipo_d", tipo);
        registro.put("cantidad_d", cantidad);
        registro.put("dni_cliente_d", dni);
        bd.insert("deposito", null, registro);
        bd.close();
        dato1.setText("");
        dato2.setText("");
        dato3.setText("");
        datosaldo.setText("");
        Toast.makeText(this, "Se cargaron los datos de la persona",
                Toast.LENGTH_SHORT).show();

    }
    @OnClick(R.id.btnmostrarsaldo)
    public void setButtontwo(View v) {
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this,"Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = da1;
        Cursor deposito1 = bd.rawQuery("select cantidad_d from deposito where dni_cliente_d="+dni,null);
        if(deposito1.getCount()==0){
            datt1 = 0;
        }else {
            Cursor deposito2 = bd.rawQuery("select sum(cantidad_d) from deposito where dni_cliente_d="+dni,null);
            if(deposito2.moveToFirst()){
                datt1 = Integer.parseInt(deposito2.getString(0));
            }
        }
        Cursor retiro1 = bd.rawQuery("select cantidad_r from retiro where dni_cliente_r="+dni,null);
        if (retiro1.getCount()==0){
            datt2 = 0;
        }else {
            Cursor retiro2 = bd.rawQuery("select sum(cantidad_r) from retiro where dni_cliente_r="+dni,null);
            if(retiro2.moveToFirst()){
                datt2 = Integer.parseInt(retiro2.getString(0));
            }
        }
        bd.close();
        saldo = datt1-datt2;
        datosaldo.setText(Integer.toString(saldo));
    }
    public void inicioir(View view){
        Intent intent = new Intent(Main5Activity.this,Main3Activity.class);
        startActivity(intent);
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendarioone.set(Calendar.YEAR, year);
            calendarioone.set(Calendar.MONTH, monthOfYear);
            calendarioone.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            actualizarInput();
        }

    };

    private void actualizarInput() {
        String formatoDeFecha = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(formatoDeFecha, Locale.US);

        dato1.setText(sdf.format(calendarioone.getTime()));
    }
}
