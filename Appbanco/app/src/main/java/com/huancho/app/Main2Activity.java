package com.huancho.app;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main2Activity extends AppCompatActivity {
    @BindView(R.id.cliente_dni) EditText et1;
    @BindView(R.id.cliente_nombre) EditText et2;
    @BindView(R.id.cliente_apellidos) EditText et3;
    @BindView(R.id.cliente_cuenta) EditText et4;
    @BindView(R.id.cliente_passw) EditText et5;
    @BindView(R.id.txtdni) TextInputLayout tildni;
    @BindView(R.id.txtnombre) TextInputLayout tilnombre;
    @BindView(R.id.txtapellidos) TextInputLayout tilapellidos;
    @BindView(R.id.txtcuenta) TextInputLayout tilcuenta;
    @BindView(R.id.txtpass) TextInputLayout tilpass;
    private Cursor fila;

    @BindView(R.id.btnregistrar) Button buttonregistrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        et1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tildni.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilnombre.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilapellidos.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilcuenta.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilpass.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    private boolean esDniValido(String dnis) {
        Pattern patron = Pattern.compile("^[0-9]+$");
        if (!patron.matcher(dnis).matches() || dnis.length() > 8) {
            tildni.setError("Debe ser 8 digitos");
            return false;
        } else {
            tildni.setError(null);
        }

        return true;
    }
    private boolean esNombreValido(String nombres) {
        Pattern patron = Pattern.compile("^[a-zA-ZáÁéÉíÍóÓúÚñÑ\\\\s ]+$");
        if (!patron.matcher(nombres).matches() || nombres.length() > 30) {
            tilnombre.setError("Solo se permite letras");
            return false;
        } else {
            tilnombre.setError(null);
        }
        return true;
    }
    private boolean esApellidoValido(String apellidos) {
        Pattern patron = Pattern.compile("^[a-zA-ZáÁéÉíÍóÓúÚñÑ\\\\s ]+$");
        if (!patron.matcher(apellidos).matches() || apellidos.length() > 30) {
            tilapellidos.setError("Solo se permite letras");
            return false;
        } else {
            tilapellidos.setError(null);
        }
        return true;
    }
    private boolean esUsuarioValido(String usua) {
        Pattern patron = Pattern.compile("^[a-zA-ZáÁéÉíÍóÓúÚñÑ\\\\s ]+$");
        if (!patron.matcher(usua).matches() || usua.length() > 30) {
            tilcuenta.setError("Solo se permite letras");
            return false;
        } else {
            tilcuenta.setError(null);
        }
        return true;
    }
    private boolean esContrasenaValido(String pass) {
        Pattern patron = Pattern.compile("^[0-9-a-zA-Z -]+$");
        if (!patron.matcher(pass).matches() || pass.length() > 30) {
            tilpass.setError("Solo se permite letras y numeros");
            return false;
        } else {
            tilpass.setError(null);
        }
        return true;
    }

    @OnClick(R.id.btnregistrar)
    public void buttonregistrar(View v) {

        String dnis = tildni.getEditText().getText().toString();
        String nombres = tilnombre.getEditText().getText().toString();
        String apellidos = tilapellidos.getEditText().getText().toString();
        String usua = tilcuenta.getEditText().getText().toString();
        String pass = tilpass.getEditText().getText().toString();

        boolean a = esDniValido(dnis);
        boolean b = esNombreValido(nombres);
        boolean c = esApellidoValido(apellidos);
        boolean d = esUsuarioValido(usua);
        boolean f = esContrasenaValido(pass);

        BD admin = new BD(this,"Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = et1.getText().toString();
        String nombre = et2.getText().toString();
        String apellido = et3.getText().toString();
        String cuenta = et4.getText().toString();
        String contrasena = et5.getText().toString();
        ContentValues registro = new ContentValues();  //es una clase para guardar datos
        registro.put("dni", dni);
        registro.put("nombre", nombre);
        registro.put("apellido", apellido);
        registro.put("usuario", cuenta+"@gmail.com");
        registro.put("password", contrasena);

        try{
            bd.insert("cliente", null, registro);
            bd.close();
            if (a && b && c && d && f){
                Toast.makeText(this, "se ha registrado correctamente", Toast.LENGTH_SHORT).show();
               // Intent intent = new Intent(this, MainActivity.class);
               // this.startActivity(intent);
                startActivity(new Intent(this,MainActivity.class));
                finish();
            }



        }catch (SQLException e){
            Toast.makeText(this, "No se registro adecuandamente"+e,
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void consultar_consul(View v) {
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase(); //Create and/or open a database that will be used for reading and writing.
        String dni = et1.getText().toString();
        Cursor fila = bd.rawQuery(  //devuelve 0 o 1 fila //es una consulta
                "select nombre,apellido,usuario,password  from cliente where dni=" + dni, null);
        if (fila.moveToFirst()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
            et2.setText(fila.getString(0));
            et3.setText(fila.getString(1));
            et4.setText(fila.getString(2));
            et5.setText(fila.getString(3));
        } else
            Toast.makeText(this, "No existe el cliente con dicho dni" ,
                    Toast.LENGTH_SHORT).show();
        bd.close();

    }

    public void eliminar_elim(View v) {
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = et1.getText().toString();
        int cant = bd.delete("cliente", "dni=" + dni, null); // (votantes es la nombre de la tabla, condición)
        bd.close();
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et5.setText("");
        if (cant == 1)
            Toast.makeText(this, "Se borró la persona con dicho documento",
                    Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "No existe una persona con dicho documento",
                    Toast.LENGTH_SHORT).show();
    }

    public void modificar_modif(View v) {
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = et1.getText().toString();
        String nombre = et2.getText().toString();
        String apellido = et3.getText().toString();
        String cuenta = et4.getText().toString();
        String contra = et5.getText().toString();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("apellido", apellido);
        registro.put("usuario", cuenta);
        registro.put("password", contra);
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et5.setText("");
        int cant = bd.update("cliente", registro, "dni=" + dni, null);
        bd.close();
        if (cant == 1)
            Toast.makeText(this, "se modificaron los datos", Toast.LENGTH_SHORT)
                    .show();
        else
            Toast.makeText(this, "no existe una persona con dicho documento",
                    Toast.LENGTH_SHORT).show();
    }

    public void inicio_inic(View view){
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        fila = bd.rawQuery(
                "select * from cliente order by dni asc ", null);

        if (fila.moveToFirst()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
            et1.setText(fila.getString(0));
            et2.setText(fila.getString(1));
            et3.setText(fila.getString(2));
            et4.setText(fila.getString(3));
        } else
            Toast.makeText(this, "No hay registrados" ,
                    Toast.LENGTH_SHORT).show();
        bd.close();
    }

    public void anterior_ant(View view){
        try {
            if (!fila.isFirst()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
                fila.moveToPrevious();
                et1.setText(fila.getString(0));
                et2.setText(fila.getString(1));
                et3.setText(fila.getString(2));
                et4.setText(fila.getString(3));
            } else
                Toast.makeText(this, "Llego al principio de la tabla",
                        Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void siguiente_sigui(View view){
        try {
            if (!fila.isLast()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
                fila.moveToNext();
                et1.setText(fila.getString(0));
                et2.setText(fila.getString(1));
                et3.setText(fila.getString(2));
                et4.setText(fila.getString(3));
            } else
                Toast.makeText(this, "Llego al final de la tabla",
                        Toast.LENGTH_SHORT).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public void ultimo_ulti(View view){
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        Cursor fila = bd.rawQuery(
                "select * from cliente order by dni asc ", null);

        if (fila.moveToLast()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
            et1.setText(fila.getString(0));
            et2.setText(fila.getString(1));
            et3.setText(fila.getString(2));
            et4.setText(fila.getString(3));
        } else
            Toast.makeText(this, "No hay registrados" ,
                    Toast.LENGTH_SHORT).show();
        bd.close();
    }

    public void limpiar_lim(View view){
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et5.setText("");
    }
    public void ir_inicio(View view){
        Intent intent = new Intent(Main2Activity.this,MainActivity.class);
        startActivity(intent);

    }
}
