package com.huancho.app;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class Main7Activity extends AppCompatActivity {
    TextView datoww;
    String da1,da2,da3,dni_c,nomb,apell;
    ListView lista_to,lista2;
    ArrayList<String> lista,lista22;
    ArrayAdapter adapter,adapter2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main7);
        datoww = (TextView)findViewById(R.id.nomap_consult);
        lista_to = (ListView) findViewById(R.id.list_deposito);
        lista2 = (ListView) findViewById(R.id.list_retiro);

        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato22");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato33");
        datoww.setText(da2+" "+da3);
        // Tomar los datos desde la base de datos para poner en el curso y después en el adapter

    }
    public void deposito_lis(View v) {
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String dni = da1;
        ArrayList<String> lalista = new ArrayList<>();
        Cursor fila = db.rawQuery(//devuelve 0 o 1 fila //es una consulta
                "select cod_dep,fecha_d,tipo_d,cantidad_d,dni_cliente_d  from deposito  where dni_cliente_d=" + dni, null);
        if(fila.getCount()==0){
            Toast.makeText(this, "NO HAY CONTENIDO EN LA LISTA",
                    Toast.LENGTH_SHORT).show();
        }
        else
            while(fila.moveToNext()){
                String datos = fila.getString(0)+"      "+fila.getString(1)+"     "+fila.getString(2)+"      "+fila.getString(3)+"   "+fila.getString(4);
                lalista.add(datos);
                ListAdapter listAdapter1 = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,lalista);
                lista_to.setAdapter(listAdapter1);
            }

        fila.close();
    }

    public void retiro_lis(View v) {

        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String dni = da1;
        ArrayList<String> lalista = new ArrayList<>();
        Cursor fila = db.rawQuery(//devuelve 0 o 1 fila //es una consulta
                "select cod_ret,fecha_r,tipo_r,cantidad_r,dni_cliente_r  from retiro where dni_cliente_r=" + dni, null);
        if(fila.getCount()==0){
            Toast.makeText(this, "NO HAY CONTENIDO EN LA LISTA",
                    Toast.LENGTH_SHORT).show();
        }
        else
            while(fila.moveToNext()){
                String datos = fila.getString(0)+"      "+fila.getString(1)+"     "+fila.getString(2)+"      "+fila.getString(3)+"   "+fila.getString(4);
                lalista.add(datos);
                ListAdapter listAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,lalista);
                lista2.setAdapter(listAdapter);
            }
        fila.close();
    }

    public void inicio_inq(View view){
        Intent intent = new Intent(Main7Activity.this,Main3Activity.class);
        startActivity(intent);
    }
}
