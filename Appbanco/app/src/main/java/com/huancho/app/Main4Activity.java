package com.huancho.app;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.material.textfield.TextInputLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main4Activity extends AppCompatActivity {
    TextView datoww;
    @BindView(R.id.cliente_dni) EditText et1;
    @BindView(R.id.cliente_nombre) EditText et2;
    @BindView(R.id.cliente_apellidos) EditText et3;
    @BindView(R.id.cliente_cuenta) EditText et4;
    @BindView(R.id.cliente_passw) EditText et5;
    @BindView(R.id.txtdni) TextInputLayout tildni;
    @BindView(R.id.txtnombre) TextInputLayout tilnombre;
    @BindView(R.id.txtapellidos) TextInputLayout tilapellidos;
    @BindView(R.id.txtcuenta) TextInputLayout tilcuenta;
    @BindView(R.id.txtpass) TextInputLayout tilpass;
    String da1,da2,da3;

    @BindView(R.id.btnmostr) Button buttonmostar;
    @BindView(R.id.btnactualizar) Button buttonactualizar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        ButterKnife.bind(this);
        datoww = (TextView)findViewById(R.id.clien_n);
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato22");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato33");
        datoww.setText(da2+" "+da3);
        et1.setText(da1);

    }

    @OnClick(R.id.btnactualizar)
    public void buttonactualizar(View v) {

        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = et1.getText().toString();
        String nombre = et2.getText().toString();
        String apellido = et3.getText().toString();
        String cuenta = et4.getText().toString();
        String contra = et5.getText().toString();
        ContentValues registro = new ContentValues();
        registro.put("nombre", nombre);
        registro.put("apellido", apellido);
        registro.put("usuario", cuenta);
        registro.put("password", contra);
        et1.setText("");
        et2.setText("");
        et3.setText("");
        et4.setText("");
        et5.setText("");
        int cant = bd.update("cliente", registro, "dni=" + dni, null);
        bd.close();
        if ( cant == 1) {
            Toast.makeText(this, "se modificaron los datos, debes iniciar sesion nuevamente", Toast.LENGTH_SHORT)
                    .show();
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
        } else
            Toast.makeText(this, "no existe una persona con dicho documento",
                    Toast.LENGTH_SHORT).show();
    }
    @OnClick(R.id.btnmostr)
    public void buttonmostar(View v) {

        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase(); //Create and/or open a database that will be used for reading and writing.
        String dni = et1.getText().toString();
        Cursor fila = bd.rawQuery(  //devuelve 0 o 1 fila //es una consulta
                "select nombre,apellido,usuario,password  from cliente where dni=" + dni, null);
        if (fila.moveToFirst()) {  //si ha devuelto 1 fila, vamos al primero (que es el unico)
            et2.setText(fila.getString(0));
            et3.setText(fila.getString(1));
            et4.setText(fila.getString(2));
            et5.setText(fila.getString(3));
        } else
            Toast.makeText(this, "No existe el cliente con dicho dni" ,
                    Toast.LENGTH_SHORT).show();
        bd.close();
    }
    public void inicio_cli(View view){
   Intent intent = new Intent(Main4Activity .this,Main3Activity.class);
    startActivity(intent);

    }
}
