package com.huancho.app;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Main6Activity extends AppCompatActivity {
    TextView datoww,datosaldo;
    int datt1,datt2;
    int saldo;
    String da1,da2,da3,dni_c,nomb,apell;
    Calendar calendario = Calendar.getInstance();

    @BindView(R.id.fecha_ret) EditText dato1;
    @BindView(R.id.tipo_reti) EditText dato2;
    @BindView(R.id.cantidad_ret) EditText dato3;

    @BindView(R.id.btnregistroretiro) Button buttonOne;
    @BindView(R.id.btnmostrarsaldo) Button buttontwo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main6);
        ButterKnife.bind(this);
        datoww = (TextView)findViewById(R.id.retiro_din);
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato22");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato33");
        datoww.setText(da2+" "+da3);
        datosaldo = (TextView) findViewById(R.id.ver_retidaro);
        dato1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(Main6Activity.this, date, calendario
                        .get(Calendar.YEAR), calendario.get(Calendar.MONTH),
                        calendario.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    public void registrar_retir(View v) {
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this,"Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();

        String fecha = dato1.getText().toString();
        String tipo = dato2.getText().toString();
        String cantidad = dato3.getText().toString();
        String dni = da1;

        ContentValues registro = new ContentValues();  //es una clase para guardar datos
        registro.put("fecha_r", fecha);
        registro.put("tipo_r", tipo);
        registro.put("cantidad_r", cantidad);
        registro.put("dni_cliente_r", dni);
        bd.insert("retiro", null, registro);
        bd.close();
        dato1.setText("");
        dato2.setText("");
        dato3.setText("");
        datosaldo.setText("");


        Toast.makeText(this, "Se cargaron los datos de la persona",
                Toast.LENGTH_SHORT).show();
    }
    public void mostrar_reti(View v) {
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato11");
        BD admin = new BD(this,"Clientesban", null, 1);
        SQLiteDatabase bd = admin.getWritableDatabase();
        String dni = da1;
        Cursor deposito1 = bd.rawQuery("select cantidad_d from deposito where dni_cliente_d="+dni,null);
        if(deposito1.getCount()==0){
            datt1 = 0;
        }else {
            Cursor deposito2 = bd.rawQuery("select sum(cantidad_d) from deposito where dni_cliente_d="+dni,null);
            if(deposito2.moveToFirst()){
                datt1 = Integer.parseInt(deposito2.getString(0));
            }
        }
        Cursor retiro1 = bd.rawQuery("select cantidad_r from retiro where dni_cliente_r="+dni,null);
        if (retiro1.getCount()==0){
            datt2 = 0;
        }else {
            Cursor retiro2 = bd.rawQuery("select sum(cantidad_r) from retiro where dni_cliente_r="+dni,null);
            if(retiro2.moveToFirst()){
                datt2 = Integer.parseInt(retiro2.getString(0));
            }
        }
        bd.close();
        saldo = datt1-datt2;
        datosaldo.setText(Integer.toString(saldo));
    }
    public void ir_inicio_Re(View view){
        Intent intent = new Intent(Main6Activity.this,Main3Activity.class);
        startActivity(intent);
    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            calendario.set(Calendar.YEAR, year);
            calendario.set(Calendar.MONTH, monthOfYear);
            calendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            actualizarInput();
        }

    };

    private void actualizarInput() {
        String formatoDeFecha = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(formatoDeFecha, Locale.US);

        dato1.setText(sdf.format(calendario.getTime()));
    }
}
