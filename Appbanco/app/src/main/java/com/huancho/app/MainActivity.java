package com.huancho.app;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.text.TextWatcher;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.etuser) EditText dato11;
    @BindView(R.id.etpass) EditText dato22;

    @BindView(R.id.txtusername) TextInputLayout tilusername;
    @BindView(R.id.txtpassword) TextInputLayout tilcontrasena;

    @BindView(R.id.btnlogin)
    Button buttonlogin;

    @BindView(R.id.btnregistro)
    Button buttonregistro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        dato11.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilusername.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        dato22.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tilcontrasena.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private boolean esUsuarioValido(String usuarios) {
        Pattern patron = Pattern.compile("^[a-zA-ZáÁéÉíÍóÓúÚñÑ\\\\s ]+$");
        if (!patron.matcher(usuarios).matches() || usuarios.length() > 30) {
            tilusername.setError("Solo se permite letras");
            return false;
        } else {
            tilusername.setError(null);
        }
        return true;
    }
    private boolean esContrasenaValido(String contrasenas) {
        Pattern patron = Pattern.compile("^[0-9-a-zA-Z -]+$");
        if (!patron.matcher(contrasenas).matches() || contrasenas.length() > 30) {
            tilcontrasena.setError("Solo se permite letras y numeros");
            return false;
        } else {
            tilcontrasena.setError(null);
        }
        return true;
    }
    @OnClick(R.id.btnlogin)
    public void buttonlogin(View v) {
        String usuarios = tilusername.getEditText().getText().toString();
        String contrasenas = tilcontrasena.getEditText().getText().toString();
        boolean a = esUsuarioValido(usuarios);
        boolean b = esContrasenaValido(contrasenas);

        BD admin = new BD(this, "Clientesban", null, 1);
        SQLiteDatabase db = admin.getWritableDatabase();
        String usuario1 = dato11.getText().toString();
        String comple = "@gmail.com";
        String usuario = usuario1+comple;
        String contrasena = dato22.getText().toString();
        Cursor FILA = db.rawQuery("SELECT usuario,password,dni,nombre,apellido  FROM cliente WHERE usuario='" + usuario + "' and password ='" + contrasena + "'", null);

        if (FILA.moveToFirst() == true && a && b) {
            String usua = FILA.getString(0);
            String pass = FILA.getString(1);
            String dni_c = FILA.getString(2);
            String nomb = FILA.getString(3);
            String apell = FILA.getString(4);
            if (usuario.equals(usua) && contrasena.equals(pass)) {
                //si son iguales entonces vamos a otra ventana
                //Menu es una nueva actividad empty
                Toast.makeText(this, "Bienvenido Cliente  = " + usua+" !!!",
                        Toast.LENGTH_SHORT).show();
                Intent ven = new Intent(MainActivity.this, Main3Activity.class);
                ven.putExtra("dato1",dni_c);
                ven.putExtra("dato2",nomb);
                ven.putExtra("dato3",apell);
                startActivity(ven);
                finish();

                dato11.setText("");
                dato22.setText("");
            }
        } else {
            Toast.makeText(this, "Usuario no valido",
                    Toast.LENGTH_SHORT).show();
            db.close();
        }
    }
    @OnClick(R.id.btnregistro)
    public void buttonregistro(View view){
       // Intent intent = new Intent(MainActivity.this,Main2Activity.class);
       // startActivity(intent);
        startActivity(new Intent(this,Main2Activity.class));
        finish();
    }
}
