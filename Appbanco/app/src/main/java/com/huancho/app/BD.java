package com.huancho.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class BD extends SQLiteOpenHelper {
    public BD(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("create table cliente(" +
                "dni integer primary key," +
                "nombre text, " +
                " apellido text , " +
                "usuario text ," +
                "password text);");

        sqLiteDatabase.execSQL("create table deposito(" +
                "cod_dep integer primary key," +
                "fecha_d text, " +
                "tipo_d text , " +
                "cantidad_d integer ," +
                "dni_cliente_d integer," +
                "  FOREIGN KEY(dni_cliente_d) references cliente(dni));");

        sqLiteDatabase.execSQL("create table retiro(" +
                "cod_ret integer primary key autoincrement," +
                "fecha_r text, " +
                "tipo_r text , " +
                "cantidad_r integer ," +
                "dni_cliente_r integer," +
                "  FOREIGN KEY(dni_cliente_r) references cliente(dni));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("drop table if exists cliente");
        sqLiteDatabase.execSQL("create table cliente(" +
                "dni integer primary key," +
                "nombre text, " +
                " apellido text , " +
                " usuario text ," +
                "password text," +
                "cantidad integer);");

        sqLiteDatabase.execSQL("drop table if exists deposito");
        sqLiteDatabase.execSQL("create table deposito(" +
                "cod_dep integer primary key autoincrement," +
                "fecha_d text, " +
                "tipo_d text , " +
                "cantidad_d integer ," +
                "dni_cliente_d integer," +
                "  FOREIGN KEY(dni_cliente_d) references cliente(dni));");
        sqLiteDatabase.execSQL("drop table if exists retiro");
        sqLiteDatabase.execSQL("create table retiro(" +
                "cod_ret integer primary key autoincrement," +
                "fecha_r text, " +
                "tipo_r text , " +
                "cantidad_r integer ," +
                "dni_cliente_r integer," +
                " FOREIGN KEY(dni_cliente_r) references cliente(dni));");
    }
}
