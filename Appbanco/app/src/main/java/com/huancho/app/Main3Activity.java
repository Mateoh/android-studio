package com.huancho.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Main3Activity extends AppCompatActivity {
    TextView datoo;
    String da1,da2,da3,dni_c,nomb,apell;
    @BindView(R.id.imgcliente) ImageView buttoncliente;
    @BindView(R.id.imgdeposito) ImageView buttondeposito;
    @BindView(R.id.imgretiro) ImageView buttonretiro;
    @BindView(R.id.imgconsulta) ImageView buttonconsulta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
        ButterKnife.bind(this);
        datoo = (TextView)findViewById(R.id.nombre_apell);

        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato1");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato2");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato3");
        datoo.setText(da2+" "+da3);

    }
    @OnClick(R.id.imgcliente)
    public void buttoncliente(View view){
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato1");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato2");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato3");
        dni_c = da1;
        nomb = da2;
        apell = da3;
        Intent intent = new Intent(Main3Activity.this,Main4Activity.class);
        intent.putExtra("dato11",dni_c);
        intent.putExtra("dato22",nomb);
        intent.putExtra("dato33",apell);
        startActivity(intent);
    }
    @OnClick(R.id.imgdeposito)
    public void buttondeposito(View view){
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato1");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato2");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato3");
        dni_c = da1;
        nomb = da2;
        apell = da3;
        Intent intent = new Intent(Main3Activity.this,Main5Activity.class);
        intent.putExtra("dato11",dni_c);
        intent.putExtra("dato22",nomb);
        intent.putExtra("dato33",apell);
        startActivity(intent);
    }
    @OnClick(R.id.imgretiro)
    public void buttonretiro(View view){
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato1");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato2");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato3");
        dni_c = da1;
        nomb = da2;
        apell = da3;
        Intent intent = new Intent(Main3Activity.this,Main6Activity.class);
        intent.putExtra("dato11",dni_c);
        intent.putExtra("dato22",nomb);
        intent.putExtra("dato33",apell);
        startActivity(intent);
    }
    @OnClick(R.id.imgconsulta)
    public void buttonconsulta(View view){
        Bundle bundle1 = this.getIntent().getExtras();
        da1=bundle1.getString("dato1");
        Bundle bundle2 = this.getIntent().getExtras();
        da2=bundle2.getString("dato2");
        Bundle bundle3 = this.getIntent().getExtras();
        da3=bundle3.getString("dato3");
        dni_c = da1;
        nomb = da2;
        apell = da3;
        Intent intent = new Intent(Main3Activity.this,Main7Activity.class);
        intent.putExtra("dato11",dni_c);
        intent.putExtra("dato22",nomb);
        intent.putExtra("dato33",apell);
        startActivity(intent);
    }
    public void mant_salir(View view){
        Intent intent = new Intent(Main3Activity.this,MainActivity.class);
        startActivity(intent);
    }
}
